# GitLab
GitLab is a web-based Git repository manager that allows to host your project on a remote repositories.

- GitLab is a complete DevOps platform, delivered as a single application.
- It provides hosting services for your projects for free if private.
- It provides wiki, issue-tracking and continuous integration/continuous deployment pipeline features.
